#!/bin/sh

/usr/sbin/sshd -D &

id_rsa="/root/.ssh/id_rsa"
id_rsap="/root/.ssh/id_rsa.pub"

if [ ! -f "$id_rsa" ] || [ ! -f "$id_rsap" ]; then
    ssh-keygen -t rsa -N "" -f "$id_rsa"
    echo "SSH key pair generated."
else
    echo "SSH key pair already exists."
fi

if [ -f "$id_rsap" ]; then
    echo "#######[PUBLIC_KEY]#######"
    cat "$id_rsap"
    echo "##########################"
    echo "[NOTE] If the key pair is new generated, send the PUBLIC_KEY or id_rsa.pub file to the middle server."
else
    echo "[ERROR] Public key file does not exist."
fi

REMOTE_HOST=$1
REMOTE_PORT=$2
REVERS_PORT=$3
LISTEN_PORT=$4

autossh -M $LISTEN_PORT -fCN $REMOTE_HOST -p $REVERS_PORT
autossh -M $LISTEN_PORT -CNR *:$REVERS_PORT:localhost:22 $REMOTE_HOST -p $REMOTE_PORT

