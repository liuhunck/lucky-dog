#!/bin/bash

if [ "$#" -lt 4 ] || [ "$#" -gt 5 ]; then
    echo "Usage: $0 REMOTE_USER REMOTE_HOST REMOTE_PORT PROXY_PORT [LISTEN_PORT]"
    exit 1
fi
    
RE_HOST="$1@$2"
RE_PORT=$3
PR_PORT=$4

if [ "$#" -eq 5 ]; then
    LS_PORT=$5
else
    LS_PORT=0
fi

docker run -it --restart unless-stopped --name proxy_to_$1_$2_${RE_PORT}_${PR_PORT} xbx/luckyd_s $RE_HOST $RE_PORT $PR_PORT $LS_PORT

exit 0
