# LuckyDog

#### 介绍
这是一个基于OpenSSH的转发服务系统，包括Windows客户端，Docker中间服务器端，Docker服务器端。

支持**跨子网代理**和直接代理。

#### 软件架构
##### win-client

基于 **WinApi** 在windows 平台上搭建的转发客户端，用于将本地转发请求发送到服务器端。

##### docker-mid-server

搭建在Docker中的转发服务器，用于处理客户端的转发请求。

##### **docker-server**

用于服务器端在子网的代理。


#### 安装教程

##### **win-client**

- 在`LuckyDog.cpp`中自定义结点相关属性
- 启用`VS`开发环境
- 编译资源文件：`rc DialogBox.rc`
- 编译`proxy_core`：`cl /c /EHsc proxy_core.cpp`
- 编译`LuckyDog`：`cl LuckyDog.cpp proxy_core.obj DialogBox.res user32.lib shell32.lib advapi32.lib`
- 修改`setup.iss`中相关路径
- 用`Inno`运行`setup.iss`脚本可以即可编译出安装包

##### **docker**

- `docker build -t xbx/sshd .`用于生成基础`docker images`
- `id_rsa.pub`可以存放管理员账号的公钥

##### **docker-mid-server**

- `cd docker-mid-server`
- `docker build -t xbx/luckyd_ms .`
- `./run.sh SSH_PORT PROXY_PORT0 [PROXY_PROT1...]`
    - 直接创建相应的容器，`SSH_PORT`是宿主机上映射到容器内`ssh`服务的端口号
    - `PROXY_PORT0 PROXY_PORT1`是一系列宿主机开放到容器内的端口，用于接收各种转发服务的端口

- 可以在`~/.ssh/proxy_mid_server_{PORT}/authorized_keys`自行添加用户公钥

##### **docker-server**

- `cd docker-server`
- `docker build -t xbx/luckyd_s .`
- `./run REMOTE_USER REMOTE_HOST REMOTE_PORT PROXY_PORT [LISTEN_PORT]`
    - `REMOTE_USER`远程连接的用户名，如`root`
    - `REMOTE_HOST`远程连接的主机名或IP
    - `REMOTE_PORT`远程连接主机开放`SSH`服务的端口
    - `PROXY_PORT`转发服务占用的端口
    - `LISTEN_PORT`保持连接用的监听端口，默认为0即可
    - 注意如果新生成了密钥，要将公钥添加至中间服务器

