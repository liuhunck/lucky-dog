#ifndef RESOURCE_H
#define RESOURCE_H
#include <windows.h>

#define IDC_STATIC -1

// Dialog box ID
#define IDD_INPUT_DIALOG 101

// Control IDs
#define IDC_EDIT1 1001
#define IDOK 1
#define IDCANCEL 2

// Icon
#define MY_ICON 3

#endif // RESOURCE_H
