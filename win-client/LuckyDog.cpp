#include <windows.h>
#include <shellapi.h>
#include "resource.h"
#include "proxy_core.h"

#define WM_TRAYICON (WM_USER + 1)
#define NODE_NUMBER 3

NOTIFYICONDATA nid;
HMENU hMenu;
HWND hwnd;

int barking = 0;
int selected_node=-1;

const std::string Node_Table[] = {
    "Custom Server",
    "* Custom Server",
    "HNU Inner No.1",
    "* HNU Inner No.1",
    "HNU Outer No.1",
    "* HNU Outer No.1",
    "HK (3M) No.1",
    "* HK (3M) No.1"
};

const char* bark_table[] = {
    "____No bark____",
    "___Try to bark___",
    "__Try to shut up__",
    "___Barkiiiiing___"
};

ThreadData* data;

ThreadData Node_Data[NODE_NUMBER + 1] = {
    ThreadData{0, 0, "", "", Node_Table[0]},
    ThreadData{10086, 2222, "root", "111.11.111.111", Node_Table[2]},
    ThreadData{10086, 2222, "root", "111.11.111.11", Node_Table[4]},
    ThreadData{10086, 2222, "root", "111.11.111.11", Node_Table[6]}
};

// Dialog
INT_PTR CALLBACK DlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
        case WM_COMMAND:
            switch (LOWORD(wParam)) {
                case IDOK: {
                    char buffer[256];
                    GetDlgItemText(hwndDlg, IDC_EDIT1, buffer, sizeof(buffer));
                    MessageBox(hwndDlg, buffer, "You entered", MB_OK);
                    EndDialog(hwndDlg, IDOK);
                    return TRUE;
                }
                case IDCANCEL:
                    EndDialog(hwndDlg, IDCANCEL);
                    return TRUE;
            }
            break;
    }
    return FALSE;
}

HMENU MyCreatePopupMenu()
{
    HMENU hMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING, 11, Node_Table[2].c_str());
    AppendMenu(hMenu, MF_STRING, 12, Node_Table[4].c_str());
    AppendMenu(hMenu, MF_STRING, 13, Node_Table[6].c_str());
    AppendMenu(hMenu, MF_STRING, 10, Node_Table[0].c_str());
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_STRING, 1, bark_table[barking]);
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_STRING, 2, "Information");
    AppendMenu(hMenu, MF_STRING, 3, "Exit");
    return hMenu;
}

bool bfirst=true;
POINT pt;
void ShowMenu() {
    if (bfirst) GetCursorPos(&pt);
    bfirst = false;
    ModifyMenu(hMenu, 1, MF_STRING, 1, bark_table[running << 1 | barking]);
    SetForegroundWindow(hwnd);
    TrackPopupMenu(hMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, pt.x, pt.y, 0, hwnd, NULL);
}

void ShowInfo() {
    MessageBox(NULL, ("ProxyEnable: " + AskProxySettings(0) + "\n" +
                "ProxyServer: " + AskProxySettings(1) + "\n" +
                "PublicKey  : " + GetPublicKey() + "\n"
                "Note: The PublicKey has been automatically copied to the clipboard").c_str(), "Information", MB_OK);
    DealErr(SetClipboardText(GetPublicKey()));
}

void ShowContact() {
    MessageBox(NULL, "If you have any questions about this proxy client, wish to upgrade your service, or want to contribute to the project, please contact us at liuhunck@qq.com.", "Contact Information", MB_OK);
}

// Handle Menu Command
void HandleMenuCommand(WPARAM wParam)
{
    switch (LOWORD(wParam))
    {
    case 1:
        if (barking) {
            barking = 0;
            if(DealErr(StopConnect()))
                barking = 1;
        }
        else {
            barking = 1;
            if(DealErr(StartConnect(data)))
                barking = 0;
        }
        ShowMenu();
        break;
    case 2:
        ShowInfo();
        break;
    case 3:
        DealErr(StopConnect());
        PostQuitMessage(0);
        break;
    case 10:
        /* DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_INPUT_DIALOG), hwnd, DlgProc); */
        MessageBox(NULL, "Please upgrade your service.", "Empty", MB_OK);
        ShowMenu();
        break;
    default:
        if (wParam < 11 || wParam > NODE_NUMBER + 10) break;
        if(selected_node == (int)(wParam - 10) && barking) break;
        if (selected_node != -1 && selected_node != (int)(wParam - 10)) {
            barking = 0;
            if(!DealErr(StopConnect())) {
                ModifyMenu(hMenu, selected_node + 10, MF_STRING, selected_node + 10, Node_Table[selected_node << 1].c_str());
            }
            else {
                barking = 1;
                ShowMenu();
                break;
            };
        }
        selected_node = wParam - 10;
        data = Node_Data + selected_node;
        barking = 1;
        ModifyMenu(hMenu, wParam, MF_STRING, wParam, Node_Table[selected_node << 1 | 1].c_str());
        if(DealErr(StartConnect(data)))
        {
            barking = 0;
            ShowMenu();
        }
        break;
    }
}

// Window Process Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg) {
        case WM_TRAYICON:
            if (lParam == WM_RBUTTONDOWN)
            {
                ShowContact();
            }
            else if (lParam == WM_LBUTTONDOWN)
            {
                ShowMenu();
            }
            break;
        case WM_COMMAND:
            HandleMenuCommand(wParam);
            break;
        case WM_DESTROY:
            Shell_NotifyIcon(NIM_DELETE, &nid);
            DestroyMenu(hMenu);
            PostQuitMessage(0);
            break;
    }
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    if(DealErr(Initialization())) return FALSE;

    WNDCLASSEX wcex;
    MSG msg;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "MyWindowClass";
    wcex.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wcex);

    hwnd = CreateWindow("MyWindowClass", "My Window", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
    if (!hwnd) return FALSE;

    memset(&nid, 0, sizeof(nid));
    nid.cbSize = sizeof(nid);
    nid.hWnd = hwnd;
    nid.uID = 1;
    nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
    nid.uCallbackMessage = WM_TRAYICON;
    nid.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    lstrcpy(nid.szTip, "LuckyDog Luck You");

    Shell_NotifyIcon(NIM_ADD, &nid);

    hMenu = MyCreatePopupMenu();

    ShowInfo();

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}
