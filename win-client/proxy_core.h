#ifndef _PROXY_CORE_
#define _PROXY_CORE_

#include <iostream>
#include <windows.h>

namespace std {
    template<typename T>
    string to_string(const T& value) {
        if (value == 0) return "0";
        T x = value;
        string res = "";
        bool t = false;
        if (x < 0) x = -x, t = true;
        while(x) {
            res.push_back(x % 10 + '0');
            x /= 10;
        }
        if (t) res.push_back('-');
        reverse(res.begin(), res.end());
        return res;
    }
}


#define ERR_SUCCESS 0
#define ERR_UNA_REG 1
#define ERR_UNA_THD 2
#define ERR_UNA_SSH 3
#define ERR_NOCHOSE 4
#define ERR_UNA_CLP 5
#define ERR_UNA_INI 6

struct ThreadData {
    unsigned int local_port;
    unsigned int remote_port;
    std::string name;
    std::string addr;
    std::string node_name;
};

int Initialization();
int DealErr(int err);

std::string AskProxySettings(int option);
int SetProxySettings(bool enable, const std::string& proxyServer="");

std::string GetPublicKey();

extern int running;
int StartConnect(const ThreadData* data);
int StopConnect();

int SetClipboardText(const std::string& text);

#endif // _PROXY_CORE_
