#include "proxy_core.h"

int running=0;

const char* errorMessage[6] = {
    "Can't access to your registry. Please check registry permissions or try running the application as an administrator.",
    "Unable to create a thread. Ensure you have the necessary permissions or try running the application as an administrator.",
    "Unable to connect to the server. Please check your network settings or ensure that the public key has been uploaded.",
    "Please select a node.",
    "Unable to copy public key to clip board.",
    "Unable Initialization. Ensure you have the necessary permissions or try running the application as an administrator."
};

int DealErr(int err) {
    switch (err) {
        case ERR_SUCCESS:
            break;
        case ERR_UNA_REG:
            MessageBox(NULL, errorMessage[0], "Registry Error", MB_OK | MB_ICONERROR);
            break;
        case ERR_UNA_THD:
            MessageBox(NULL, errorMessage[1], "Thread Error", MB_OK | MB_ICONERROR);
            break;
        case ERR_UNA_SSH:
            MessageBox(NULL, errorMessage[2], "Connect Error", MB_OK | MB_ICONERROR);
            break;
        case ERR_NOCHOSE:
            MessageBox(NULL, errorMessage[3], "Connect Error", MB_OK | MB_ICONERROR);
            break;
        case ERR_UNA_CLP:
            MessageBox(NULL, errorMessage[4], "Clip Error", MB_OK | MB_ICONERROR);
            break;
        case ERR_UNA_INI:
            MessageBox(NULL, errorMessage[5], "Clip Error", MB_OK | MB_ICONERROR);
            break;
        default:
            break;
    }
    return err;
}
bool fdestroy=false;
HANDLE hThread;
DWORD threadId;

std::string pre_path;

int Initialization() {
    char path[MAX_PATH];
    if (GetEnvironmentVariable("LOCALAPPDATA", path, sizeof(path))) 
        pre_path = std::string(path) + "\\LuckyDog\\";
    else return ERR_UNA_INI;

    std::string sk = pre_path + "cert";
    std::string pk = pre_path + "cert.pub";
    bool sk_f = (GetFileAttributes(sk.c_str()) != INVALID_FILE_ATTRIBUTES);
    bool pk_f = (GetFileAttributes(pk.c_str()) != INVALID_FILE_ATTRIBUTES);
    if (sk_f && pk_f) return 0;
    if (sk_f) DeleteFile(sk.c_str());
    if (pk_f) DeleteFile(pk.c_str());

    PROCESS_INFORMATION pi;
    STARTUPINFO si;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;

    std::string command = pre_path + "OpenSSH\\ssh-keygen.exe -t rsa -N \"\" -f " + sk;

    if (CreateProcess(NULL, (LPSTR)command.c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        DWORD waitResult = WaitForSingleObject(pi.hProcess, INFINITE);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
        if (waitResult != WAIT_OBJECT_0) 
            return ERR_UNA_INI;
    }
    else return ERR_UNA_INI;

    MessageBox(NULL, "A new public key has been generated. Upload it to the server to begin your Internet adventure.", "Initialization", MB_OK | MB_ICONINFORMATION);
    return 0;
}

DWORD WINAPI ConnectThreadMain(LPVOID lpParam) {
    ThreadData* data = (ThreadData*)(lpParam);

    std::string tmp = std::to_string(data->local_port);
    std::string command = pre_path + "OpenSSH\\ssh.exe -CND " + std::to_string(data->local_port) +
                          " -o StrictHostKeyChecking=no" + " -i " + pre_path + "cert" +
                          " -p " + std::to_string(data->remote_port) + " " +
                          data->name + "@" + data->addr;
    std::cerr << command.c_str() << std::endl;
    
    PROCESS_INFORMATION pi;
    STARTUPINFO si;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    ZeroMemory(&si, sizeof(STARTUPINFO));
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;
    while (!fdestroy) {
        if (CreateProcess(
                NULL,       // No module name (use command line)
                (LPSTR)command.c_str(), // Command line
                NULL,       // Process handle not inheritable
                NULL,       // Thread handle not inheritable
                FALSE,      // Set handle inheritance to FALSE
                0,          // No creation flags
                NULL,       // Use parent's environment block
                NULL,       // Use parent's starting directory 
                &si,        // Pointer to STARTUPINFO structure
                &pi         // Pointer to PROCESS_INFORMATION structure
        )) {
            std::cerr << "Create Connection Successfully. Process ID: " << pi.dwProcessId << std::endl;
            while (true) {
                if (fdestroy) TerminateProcess(pi.hProcess, 1);
                DWORD waitResult = WaitForSingleObject(pi.hProcess, 4000);
                if (waitResult != WAIT_TIMEOUT) break;
                if (running == 0)
                {
                    running = 1;
                    MessageBox(NULL, ("Conected to " + data->node_name + ", Enjoy the bark...").c_str(), "Connected", MB_OK | MB_ICONINFORMATION);
                }
            }
            if (running == 1)
            {
                running = 0;
                MessageBox(NULL, ("Closed conection to " + data->node_name).c_str(), "Connected", MB_OK | MB_ICONINFORMATION);
            }
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
        }
    }
    if (running == 1)
    {
        running = 0;
        MessageBox(NULL, ("Closed conection to " + data->node_name).c_str(), "Connected", MB_OK | MB_ICONINFORMATION);
    }
    return 0;
}

void joinConnect() {
    WaitForSingleObject(hThread, INFINITE);
    CloseHandle(hThread);
}

int startConnect(void *args) {
    hThread = CreateThread(
        NULL,                // Default security attributes
        0,                   // Default stack size
        ConnectThreadMain,   // Thread function
        args,               // Argument to thread function
        0,                   // Default creation flags
        &threadId            // Thread identifier
    );

    if (hThread == NULL) {
        std::cerr << "Create Thread Failed. Error: " << GetLastError() << std::endl;
        return ERR_UNA_THD;
    }

    for (int i = 0; i < 5; ++i) {
        WaitForSingleObject(hThread, 1000);
        if (running) return 0;
    }
    fdestroy = true;
    joinConnect();
    return ERR_UNA_SSH;
}

std::string AskProxySettings(int option) {
    HKEY hKey;
    LONG result;
    DWORD dataSize;

    // Open the registry key
    result = RegOpenKeyEx(HKEY_CURRENT_USER,
                          "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings",
                          0,
                          KEY_SET_VALUE | KEY_QUERY_VALUE,
                          &hKey);

    if (result != ERROR_SUCCESS) {
        std::cerr << "Failed to open registry key." << std::endl;
        return "";
    }

    std::string res = "";

    switch (option) {
        case 0:
            // Query ProxyEnable
            DWORD proxyEnable;
            dataSize = sizeof(proxyEnable);
            result = RegQueryValueEx(hKey, TEXT("ProxyEnable"), NULL, NULL, reinterpret_cast<BYTE*>(&proxyEnable), &dataSize);

            if (result == ERROR_SUCCESS) {
                std::wcout << L"ProxyEnable value: " << proxyEnable << std::endl;
                res = std::to_string((int)proxyEnable);
            } else {
                std::wcout << L"Failed to query registry value of ProxyEnable. Error: " << result << std::endl;
            }
            break;
        case 1:
            // Query ProxyServer
            char value[256];
            dataSize = sizeof(value);
            result = RegQueryValueEx(hKey, TEXT("ProxyServer"), NULL, NULL, reinterpret_cast<BYTE*>(value), &dataSize);

            if (result == ERROR_SUCCESS) {
                std::wcout << L"ProxyServer value: " << value << std::endl;
                res = value;
            } else {
                std::wcout << L"Failed to query registry value of ProxyServer. Error: " << result << std::endl;
            }
            break;
        default:
            break;
    }

    // Close the registry key
    RegCloseKey(hKey);
    return res;
}

int SetProxySettings(bool enable, const std::string& proxyServer) {
    HKEY hKey;
    LONG result;
    DWORD proxyEnable;

    // Open the registry key
    result = RegOpenKeyEx(HKEY_CURRENT_USER,
                          "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings",
                          0,
                          KEY_SET_VALUE | KEY_QUERY_VALUE,
                          &hKey);

    if (result != ERROR_SUCCESS) {
        std::cerr << "Failed to open registry key." << std::endl;
        return ERR_UNA_REG;
    }

    int res = 0;

    // Set ProxyEnable
    proxyEnable = enable ? 1 : 0;
    result = RegSetValueEx(hKey, "ProxyEnable", 0, REG_DWORD,
                           reinterpret_cast<const BYTE*>(&proxyEnable), sizeof(proxyEnable));

    if (result == ERROR_SUCCESS) {
        std::wcout << L"Set ProxyEnable to: " << proxyEnable << std::endl;
    } else {
        std::wcout << L"Failed to set registry value of ProxyEnable. Error: " << result << std::endl;
        res = ERR_UNA_REG;
    }

    // Set ProxyServer
    if (enable) {
        result = RegSetValueEx(hKey, "ProxyServer", 0, REG_SZ,
                               reinterpret_cast<const BYTE*>(proxyServer.c_str()), static_cast<DWORD>(proxyServer.size() + 1));

        if (result == ERROR_SUCCESS) {
            std::wcout << L"Set ProxyServer to: " << proxyServer.c_str() << std::endl;
        } else {
            std::wcout << L"Failed to query registry value of ProxyServer. Error: " << result << std::endl;
            res = ERR_UNA_REG;
        }
    }

    // Close the registry key
    RegCloseKey(hKey);
    return res;
}

std::string GetPublicKey() {
    HANDLE hFile = CreateFile(
        (pre_path + "cert.pub").c_str(),
        GENERIC_READ,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);

    if (hFile == INVALID_HANDLE_VALUE) {
        std::wcerr << L"Failed to open file. Error code: " << GetLastError() << std::endl;
        return "";
    }

    DWORD fileSize = GetFileSize(hFile, NULL);
    if (fileSize == INVALID_FILE_SIZE) {
        std::wcerr << L"Failed to get file size. Error code: " << GetLastError() << std::endl;
        CloseHandle(hFile);
        return "";
    }

    char* buffer = new char[fileSize + 1];
    if (!buffer) {
        std::wcerr << L"Memory allocation failed." << std::endl;
        CloseHandle(hFile);
        return "";
    }

    DWORD bytesRead;
    if (!ReadFile(hFile, buffer, fileSize, &bytesRead, NULL)) {
        std::wcerr << L"Failed to read file. Error code: " << GetLastError() << std::endl;
        delete[] buffer;
        CloseHandle(hFile);
        return "";
    }

    buffer[bytesRead] = '\0';
    std::string res(buffer);

    delete[] buffer;
    CloseHandle(hFile);

    return res;
}

int StartConnect(const ThreadData* data) {
    if (data == NULL) return ERR_NOCHOSE;

    fdestroy = false;
    int res = startConnect((void*)data);
    if (res) return res;

    std::string proxyServer = "socks=127.0.0.1:" + std::to_string(data->local_port);
    return SetProxySettings(true, proxyServer);
}

int StopConnect() {
    if(fdestroy || !running) return 0;
    fdestroy = true;
    int res = SetProxySettings(false);
    joinConnect();
    return res;
}

int SetClipboardText(const std::string& text) {
    if (!OpenClipboard(nullptr))
        return ERR_UNA_CLP;

    EmptyClipboard();

    HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, (text.size() + 1) * sizeof(char));
    if (!hGlobal) {
        CloseClipboard();
        return ERR_UNA_CLP;
    }

    char* pGlobal = static_cast<char*>(GlobalLock(hGlobal));
    if (!pGlobal) {
        GlobalFree(hGlobal);
        CloseClipboard();
        return ERR_UNA_CLP;
    }

    memcpy(pGlobal, text.c_str(), text.size());
    GlobalUnlock(hGlobal);

    SetClipboardData(CF_TEXT, hGlobal);

    CloseClipboard();
    return 0;
}
