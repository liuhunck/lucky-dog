FROM alpine:latest

LABEL maintainer="Xbx <liuhunck@gmail.com>" \
      version="1.0" \
      description="This image contains OpenSSH server on Alpine Linux."

# Install OpenSSH and other dependencies
RUN apk update && apk add openssh

# Create a user and set up SSH
RUN mkdir /root/.ssh && \
    chmod 700 /root/.ssh

# Copy your public key to the container
COPY id_rsa.pub /root/.ssh/authorized_keys
RUN chmod 600 /root/.ssh/authorized_keys

# Generate SSH host keys
RUN ssh-keygen -A

# Expose the SSH port
EXPOSE 22

# Start the SSH service
CMD ["/usr/sbin/sshd", "-D"]
