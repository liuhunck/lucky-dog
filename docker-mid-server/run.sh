#!/bin/bash

if [ "$#" -lt 2 ]; then
	echo "Usage: $0 SSH_PORT PROXY_PORT0 [PROXY_PORT1...]"
	exit 1
fi

PROXY_PORTS="-p $1:22"

for arg in ${@:2}; do
	PROXY_PORTS="$PROXY_PORTS -p $arg:$arg"
done

docker run -itd --restart unless-stopped --name proxy_mid_server_$1 \
	-v ~/.ssh/proxy_mid_server_$1:/root/.ssh $PROXY_PORTS xbx/luckyd_ms

exit 0

